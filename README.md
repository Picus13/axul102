# Axul 102

## La présentation
Dans le cadre de [l'Université Populaire du Pays d'Aix](http://up-aix.com/), l'[AXUL](https://axul.org/) intervient pour une présentation sur le thème de "Pourquoi promouvoir le logiciel libre".  
Cet extrait de présentation traite de la partie des modéles économiques dans le logiciel libre, et la contribution participative

Les notes de préparation sont disponible en cliquant [ici](note.md)
## AccesSlide
Cette présentation repose sur le framework AccesSlide, écrit en HTML/CSS/JS. Voici les liens pour consulter la documentation en [Version française](READMEFR.md)/[English Version](README_old.md)

### Voir la présentation

La présentation est accessible en cliquant sur le lien suivant [Présentation](https://picus13.frama.io/axul102/index.htm)

## Remerciements
* [l'Université Populaire du Pays d'Aix](http://up-aix.com/) de nous accueillir !
* [Undraw.co](https://undraw.co) pour toutes les illustrations 
* [Access42](http://www.access42.net) pour leur super super framework !
* @Pimprenelle pour les corrections d'orthographe et :tada: pour ses première contributions !

## Licence

Tout le code fourni par [Access42](http://www.access42.net) est sous licence GNU GENERAL PUBLIC LICENSE V3.
